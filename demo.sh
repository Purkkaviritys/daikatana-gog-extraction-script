#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o nounset

declare ARGS_1="${1}"
declare -r NAMES=("Mikko" "Jukka" "Pekko")
declare -r TMP_PATH="./tmp_data"

_usage() {
	echo "You'll need to input at least one argument of any kind."

	exit 1
}

_main() {

	echo "Script filename: '${0}'"

	if [ ! -z "${ARGS_1}" ]; then
		echo "Script arguments: '${1}'"
	else
		echo "Error: No arguments passed!"
		_usage

		exit 1
	fi

	printf '%s' "Script names: "

	for i in "${NAMES[@]}"; do
		printf '"%s" ' "${i}"
	done

	printf '\n%s\n' "Info: I'll create a new tmp directory here."

	if [ ! -d "${TMP_PATH}" ]; then
		mkdir -p "${TMP_PATH}"
		mkdir -p ./lol
	fi

	if [ -d "${TMP_PATH}" ]; then
		mkdir -p ./lol/1
	else
		echo "Info: ${TMP_PATH} already exists!"
	fi

	# Test for system-wide innoextract install.
	if command -v innoextract >/dev/null 2>&1; then
		echo "Info: Your system doesn't seem to have innoextract installed." \
			"But that's ok, we'll use our own."
	else
		echo "Info: Found system-wide install of innoextract from your system." \
			"Let's see if it meets our version requirements."

		local -r INNO_VERSION="$(./innoextract --silent --version)"
		local -r REQUIRED_INNO_VERSION="1.8"

		if [ ! "$(printf '%.1f\n' "${INNO_VERSION}")" \> "${REQUIRED_INNO_VERSION}" ]; then
			echo "Info: Damn son, you have newer than what we have."
		else
			echo "Info: Bummer, we need a newer than what you have."
		fi
	fi

	echo "Info: Doing my work stuff now."

	if ! command -v innoextract >/dev/null 2>&1; then
		# echo "Info: Searching for necesseary programs."
		# sudo dnf search innoextract
		echo "Info: Installing necesseary programs."
		# sudo dnf install innoextract
	fi

	if [ -d "${TMP_PATH}" ]; then
		echo "Warning: I'll delete the old tmp directory."
		rm -r "${TMP_PATH}"
	fi

	exit 0
}

_main "$@"
