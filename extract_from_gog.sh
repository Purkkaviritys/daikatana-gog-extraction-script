#!/usr/bin/env bash

# Fail on error, ensure errors in statements with pipes cause a failure,
# and bail out if reading unset variables.
set -o errexit
set -o pipefail
# set -o nounset

# While debugging, print executed clauses.
[[ "${DEBUG}" == 'true' ]] && set -o xtrace

# Global variables.
declare __SCRIPT_NAME="${0}"
declare __INSTALLER_PATH="${1}"
declare -r __TEMP_PATH="./install_tmp"

# Functions.
_print_usage() {
	printf "Usage: %s /path/to/setup_daikatana_2.0.0.3.exe\n
			" \
			"${__SCRIPT_NAME}" \
			| while read -r line ; do echo -e "$line" ; done

}

_print_bug_info() {
	printf "Error: Please report this as a bug to the Daikatana 1.3 Team's issue tracker...\n\
	https://bitbucket.org/daikatana13/daikatana/issues\n\n\n"\
	| while read -r line ; do echo "$line" ; done

}

# Check if the innoextract exists in system path and is its version
# is at least 1.7 and if doesn't then check if we have innoextract in dk13
# setup directory and if it is at least 15.0 version. Needs error checks!
_check_innoextract() {

	# Test for system-wide innoextract install.
	if command -v innoextract >/dev/null 2>&1; then
		echo "Info: Your system doesn't seem to have innoextract installed." \
		"But that's ok, we'll use our own."
	else
		echo "Info: Found system-wide install of innoextract from your system." \
		"Let's see if it meets our version requirements."

		local -r __INNO_VERSION="$(./innoextract --silent --version)"
		local -r __REQUIRED_INNO_VERSION="1.7"

		if [ ! "$(printf '%.1f\n' "${__INNO_VERSION}")" \> "${__REQUIRED_INNO_VERSION}" ]; then
			echo "Info: Damn son, you have newer than what we have."
		else
			echo "Info: Bummer, we need a newer than what you have."
		fi
	fi

}

_main() {

	# If no path to GOG installer was presented, print usage information.
	if [ -z "${__INSTALLER_PATH}" ]; then

		_print_usage

		echo -e "Extractor for GOG.com Daikatana installer using innoextract.\n\
				It will extract the relevant game data to this directory\n\
				so it can be used with Daikatana 1.3\n\
				\n\
				Note: This only works with the GOG.com installer,\n\
				not the CD version or any other version of Daikatana!\n
				Report bugs to https://bitbucket.org/daikatana13/daikatana/issues" \
				| while read -r line ; do echo "$line" ; done

		exit 1
	fi

	if [ ! -e "${__INSTALLER_PATH}" ]; then
		echo "Error: The file '${__INSTALLER_PATH}' doesn't exist!"

		_print_usage

		exit 1
	fi

	# TODO: Check does the path already exist and fail if it does.
	if [ -d "${__TEMP_PATH}" ]; then
		echo "Warning: ${__TEMP_PATH} already exists." \
		"Let's see if there's anything inside."

        # TODO: Can I use -z here? Was ! -n.
		if [ -z "$(find "${__TEMP_PATH}" -maxdepth 0 -type d -empty 2>/dev/null)" ]; then
			echo "Warning: There are filess inside the ${__TEMP_PATH}. \
			I'll bail out to avoid doing anything silly."

			exit 1
		fi

		echo "Info: ${__TEMP_PATH} seems to be empty."

	else
		echo "Info: Creating temporary install directory."
		mkdir "${__TEMP_PATH}"
	fi

	# Perform some checks for Innoextract.
	if _check_innoextract; then
		echo "Error: Something went wrong when checking on innoextract."
	fi

	# Extract the installer.
	echo "Extracting installer:"

	if ! ./innoextract --silent --progress --color --lowercase \
		--gog --output-dir "${__TEMP_PATH}" "${__INSTALLER_PATH}"; then

		echo "Error: Extracting failed for unknown reason!"

		_print_bug_info

		rm -r "${__TEMP_PATH}"
		exit 1
	fi

	# TODO: More error checking here.
	if [ ! -d "${__TEMP_PATH}" ]; then
		echo "Error: Unable to find install_tmp directory!"

		_print_bug_info

		echo "However, you can find the unextracted data in ${__TEMP_PATH} " \
			"Maybe you can find the data/ directory somewhere in there. " \
			"It should contain a file named pak1.pak, amongst other things."

		exit 1
	fi

	# We already have a data directory, probably with patched files,
	# copy those over data/ from installer.
	if [ -d ./data ]; then

		echo "Info: Patching installer's install_tmp/data/ directory with " \
			"contents of 1.3 patch's ./data/"

		cp -r ./data/* ${__TEMP_PATH}/data/

		echo "Removing path ./data/"
		rm -r ./data

	fi

	# TODO: This stuff should be error checked as well.
	echo "Info: Moving install_tmp/data/ from installer to ./data/"
	mv ${__TEMP_PATH}/data .

	echo "Info: Retrieving manual from installer"
	mv ${__TEMP_PATH}/*.pdf .

	# TODO: Offer option to not clean left overs.
	echo "Warning: Cleaning up unused files from installer"
	rm -r ${__TEMP_PATH}

	exit 0
}

# Execute the script.
_main "$@"
